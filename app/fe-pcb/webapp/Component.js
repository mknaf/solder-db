sap.ui.define(
    ["sap/fe/core/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("fepcb.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);