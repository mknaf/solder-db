# Solder DB CAP

This is a database for managing electronic parts for soldering,
made with SAP CAP.

## Prerequisites and recommendations

Use [Volta.sh](https://volta.sh/).

Use yeoman for frontend app scaffolding
```bash
npm install -g yo
npm install -g @sap/generator-fiori
```


npm install -g mta

