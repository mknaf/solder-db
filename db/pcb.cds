using {
    cuid,
    managed,
} from '@sap/cds/common';

using { Resistor as Resistor_ctx} from './resistor';
using { Capacitor as Capacitor_ctx } from './capacitor';

context PCB {

    entity PCB: cuid, managed {
        name: String
            @mandatory
            @assert.notNull
            ;
        resistors: Association to many Resistor_ctx.ResistorOnPCB
            on resistors.pcb = $self;
        capacitors: Association to many Capacitor_ctx.CapacitorOnPCB
            on capacitors.pcb = $self;
    }
}
