using {
    cuid,
    managed,
} from '@sap/cds/common';

using { PCB as PCB_ctx } from './pcb.cds';

context Capacitor {

    @cds.autoexpose
    entity CapacitorOnPCB: managed {
        key pcb: Association to one PCB_ctx.PCB;
        key capacitor: Association to one Capacitor;
            amount: Integer
                @assert.range: [
                    0,
                    //Infinity
                    100000000000000000
                ];
    }

    @assert.unique: { value: [ value ]}
    entity Capacitor: cuid, managed {
        value: Decimal
            @mandatory
            @assert.range: [
                0,
                //Infinity
                100000000000000000
            ];
        price: Decimal
            @assert.range: [
                0,
                //Infinity
                100000000000000000
            ];
        type: String enum {
            electrolytic;
            ceramic;
            paper;
        };
        pcbs: Association to many CapacitorOnPCB
            on pcbs.capacitor = $self;
    }
}
