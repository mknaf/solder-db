using SolderService from './solder-service';

annotate SolderService.PCB with @(
    UI       : {
        // filter bar fields
        SelectionFields: [name, ],
        LineItem       : [{
            $Type            : 'UI.DataField',
            Value            : name,
            Label            : 'Name',
            ![@UI.Importance]: #High,
        }, ],
    },
);
annotate SolderService.PCB with @(
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            Label : '{i18n>Resistors.Table.Title}',
            ID : 'Resistors',
            Target : 'resistors/@UI.LineItem#Resistors',
        },
    ]
);
annotate SolderService.ResistorOnPCB with @(
    UI.LineItem #Resistors : [
        {
            $Type : 'UI.DataField',
            Value : resistor.value,
            Label : '{i18n>Resistors.Table.Value}',
        },
        {
            $Type : 'UI.DataField',
            Value : amount,
            Label : '{i18n>Resistors.Table.Amount}',
        },{
            $Type : 'UI.DataField',
            Value : resistor.price,
            Label : '{i18n>Resistors.Table.Price}',
        },]
);