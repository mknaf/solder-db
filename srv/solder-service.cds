using { PCB as PCB_ctx } from '../db/pcb';
using { Capacitor as Capacitor_ctx } from '../db/capacitor';
using { Resistor as Resistor_ctx } from '../db/resistor';

service SolderService {
    entity PCB as projection on PCB_ctx.PCB;
    entity Resistor as projection on Resistor_ctx.Resistor;
    entity Capacitor as projection on Capacitor_ctx.Capacitor;
}
