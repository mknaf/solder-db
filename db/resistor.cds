using {
    cuid,
    managed,
} from '@sap/cds/common';

using { PCB as PCB_ctx } from './pcb.cds';

context Resistor {

    entity ResistorOnPCB: managed {
        key pcb: Association to one PCB_ctx.PCB;
        key resistor: Association to one Resistor;
            amount: Integer
                @assert.range: [
                    0,
                    //Infinity
                    100000000000000000
                ];
    }

    @assert.unique: { value: [ value ]}
    entity Resistor: cuid, managed {
        value: Decimal
            @mandatory
            @assert.range: [
                0,
                //Infinity
                100000000000000000
            ];
        price: Decimal
            @assert.range: [
                0,
                //Infinity
                100000000000000000
            ];
        pcbs: Association to many ResistorOnPCB
            on pcbs.resistor = $self;
    }
}
