sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'fepcb/test/integration/FirstJourney',
		'fepcb/test/integration/pages/PCBList',
		'fepcb/test/integration/pages/PCBObjectPage'
    ],
    function(JourneyRunner, opaJourney, PCBList, PCBObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('fepcb') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onThePCBList: PCBList,
					onThePCBObjectPage: PCBObjectPage
                }
            },
            opaJourney.run
        );
    }
);